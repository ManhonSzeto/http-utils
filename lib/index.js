'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.postJSON = exports.get = undefined;

let get = exports.get = (() => {
    var _ref = _asyncToGenerator(function* (url) {
        const result = yield (0, _promiseAdapter2.default)(_request2.default)(url);
        return {
            statusCode: result[0].statusCode,
            body: result[1]
        };
    });

    return function get(_x) {
        return _ref.apply(this, arguments);
    };
})();

let postJSON = exports.postJSON = (() => {
    var _ref2 = _asyncToGenerator(function* (url, json) {
        const result = yield (0, _promiseAdapter2.default)(_request2.default)({
            url: url,
            method: 'POST',
            json: json,
            headers: { 'content-type': 'application/json' }
        });
        return {
            statusCode: result[0].statusCode,
            body: result[1]
        };
    });

    return function postJSON(_x2, _x3) {
        return _ref2.apply(this, arguments);
    };
})();

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _promiseAdapter = require('promise-adapter');

var _promiseAdapter2 = _interopRequireDefault(_promiseAdapter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }