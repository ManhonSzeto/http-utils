import request from 'request';
import promiseAdapter from 'promise-adapter';

export async function get(url) {
    const result = await promiseAdapter(request)(url);
    return {
        statusCode : result[0].statusCode,
        body : result[1]
    };
}

export async function postJSON(url, json) {
    const result = await promiseAdapter(request)({
        url : url,
        method : 'POST',
        json : json,
        headers : { 'content-type' : 'application/json' }
    });
    return {
        statusCode : result[0].statusCode,
        body : result[1]
    };
}

